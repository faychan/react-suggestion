import React, { Component } from 'react';
import './App.css';
// import Information from './info-json';

class App extends Component {

  constructor(){
    super();

    this.state={
      search: null,
      query: null,
      usersData: [
        {
          id: 92389,
          name:"Samule",
          "age":21,
          "country":"USA"
        },
        {
          id: 234123,
          name:"Sam",
          "age":21,
          "country":"USA"
        },
        {
          id: 11245,
          name:"Mark",
          "age":21,
          "country":"Africa"
        },
        {
          id: 32423,
          name:"Markus",
          "age":21,
          "country":"Africa"
        },
        {
          id: 334242,
          name:"Aayush",
          "age":21,
          "country":"India"
        },
        {
          id: 245323,
          name:"Sean",
          "age":21,
          "country":"Ireland"
        },
        {
          id: 342354,
          name:"Eduardo",
          "age":21,
          "country":"France"
        },
        {
          id: 12354,
          name:"Dustin",
          "age":21,
          "country":"Spain"
        },
        {
          id: 324,
          name:"Alexendra",
          "age":21,
          "country":"USA"
        },
        {
          id: 44534,
          name:"Lee",
          "age":21,
          "country":"China"
        },
        {
          id: 342,
          name:"Jim",
          "age":21,
          "country":"Korea"
        }
      ],
      skills: [
        {
          id: 92389,
          name:"Reactjs",
        },
        {
          id: 5849,
          name:"Vue",
        },
        {
          id: 2343,
          name:"JavaScript",
        },
        {
          id: 5453,
          name:"jQuery",
        },
        {
          id: 32423,
          name:"Redux",
        },
        {
          id: 4554,
          name:"React Native",
        },
        {
          id: 2349,
          name:"React Developer",
        },
      ],
      filteredList: [],
      isTrue: false,
    };
  }

  searchSpace=(event)=>{
    let keyword = event.target.value;
    var { skills, filteredList } = this.state;
    if (event.target.value.indexOf('#') >= -1) {
      // $(this).val(value.replace(/\#/g, ""));
      keyword = keyword.replace(/#/g, "");
    }
    // var mystring = "crt/r2002_2";
    // mystring = mystring.replace('/r','/');

    for(var i = 0; i>skills.length; i++){
      Object.keys(skills).forEach(skillName =>  {
        if(skillName[i].toLowerCase().includes(keyword.toLowerCase())){
          console.log(skillName);
        }
      }
      )
    }

    this.setState({
      filteredList,
      search:keyword, 
      query:event.target.value
    })
  }

  render(){
    return (
    <div className="container-bg">
      <textarea type="text" placeholder="Enter skill to be searched" onChange={(e)=>this.searchSpace(e)} />
    </div>
    )
  }
}

export default App;
